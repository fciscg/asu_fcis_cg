﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CGUtilities
{
    public class TestingUtilities
    {
        public static T Limex<T>(Func<T> F, int Timeout, out bool Completed, out TimeSpan ApproximateElapsedTime)
        {
            T result = default(T);
            Stopwatch sw = new Stopwatch();
            
            Thread thread = new Thread(() => result = F());
            thread.Start();
            sw.Start();
            Completed = thread.Join(Timeout + 100);//+100 for other time needed by c#
            sw.Stop();
            ApproximateElapsedTime = sw.Elapsed;
            if (!Completed) thread.Abort();
            return result;
        }

        // Overloaded method, for cases when we don't   
        // need to know if the method was terminated  
        public static T Limex<T>(Func<T> F, int Timeout, TimeSpan ApproximateElapsedTime)
        {
            bool Completed;
            return Limex(F, Timeout, out Completed, out ApproximateElapsedTime);
        }  
    }
}

/// Regarding how to use
/*
    https://web.archive.org/web/20140222210133/http://kossovsky.net/index.php/2009/07/csharp-how-to-limit-method-execution-time 
    
    bool Completed;  
    string Content = Limex(() => File.ReadAllText(@"\\unc\dir\file.ext")  
                           ,100 // milliseconds  
                           ,out Completed);  
  
    if (Completed)   
       // Do something  
    else  
      // Do something else  
 
 */