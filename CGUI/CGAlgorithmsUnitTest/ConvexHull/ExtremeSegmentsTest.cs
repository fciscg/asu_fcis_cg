﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGAlgorithms.Algorithms.ConvexHull;
using CGAlgorithms;
using CGUtilities;
using System.Collections.Generic;

namespace CGAlgorithmsUnitTest
{
    /// <summary>
    /// 15 test case, each 6.67% from the total grade
    /// Grade is 0.67 for each test case that passes where total algorithm grade is 10
    /// </summary>
    [TestClass]
    public class ExtremeSegmentsTest : ConvexHullTest
    {
        bool isCompleted;
        int result;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void InitializeTesTData()
        {
            convexHullTester = new ExtremeSegments();
            resultWriter = new System.IO.StreamWriter("TestCasesResults.txt", true);
            result = -1;
            isCompleted = false;
        }

        [TestCleanup]
        public void CleanUpTestData()
        {
            bool correctAnswer = isCompleted && result == 0;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ApproximateElapsedTime.Hours, ApproximateElapsedTime.Minutes, ApproximateElapsedTime.Seconds,
                ApproximateElapsedTime.Milliseconds);
            string errorMSG = "";

            if (result == -2)
                errorMSG = "Exception occured in your code";
            else if (result == -1)
                errorMSG = "Output is incorrect";


            bool RTE = result == -2;

            resultWriter.WriteLine(TestContext.TestName + "," + elapsedTime + "," + isCompleted + "," + correctAnswer + "," + RTE + "," + errorMSG);
            resultWriter.Close();
        }
        
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase20Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case20Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 1 (20 Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase40Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case40Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 2 (40 Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase60Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case60Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 3 (60 Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase80Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case80Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 4 (80 Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase100Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case100Points, 2000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 5 (100 Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase200Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case200Points, 6000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 6 (200 Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase400Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case400Points, 35000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 7 (400 Point)");

        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase600Points()
        {

            result = CGUtilities.TestingUtilities.Limex<int>(Case600Points, 2 * 60000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 8 (600 Point)");

        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase800Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case800Points, 4 * 60000 + 40000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 9 (800 Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsNormalTestCase1000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case1000Points, 21 * 60000 + 10000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 10 (1000 Point)");
        }

        [TestMethod]
        public void ExtremeSegmentsSpecialCase10SamePoints()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase10SamePoints, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 16 (Point)");
        }
        [TestMethod]
        public void ExtremeSegmentsSpecialCaseLine()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseLine, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 17 (Line)");
        }
        [TestMethod]
        public void ExtremeSegmentsSpecialCaseTriangle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseTriangle, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 18 (Triangle)");
        }
        [TestMethod]
        public void ExtremeSegmentsSpecialCaseCircle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseCircle, 2000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 19 (Circle)");
        }
        [TestMethod]
        public void ExtremeSegmentsSpecialCaseConvexPolygon()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseConvexPolygon, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 20 (Convex Polygon)");
        }
    }
}
