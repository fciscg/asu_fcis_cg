﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGAlgorithms.Algorithms.ConvexHull;
using CGAlgorithms;
using CGUtilities;
using System.Collections.Generic;

namespace CGAlgorithmsUnitTest
{
    /// <summary>
    /// 20 Test Cases, each 5% from the total grade.
    /// Grade is 0.05 for each test case where total algorithm grade is 10.
    /// </summary>
    [TestClass]
    public class JarvisMarchTest : ConvexHullTest
    {
        bool isCompleted;
        int result;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void InitializeTesTData()
        {
            convexHullTester = new JarvisMarch();
            resultWriter = new System.IO.StreamWriter("TestCasesResults.txt", true);
            result = -1;
            isCompleted = false;
        }

        [TestCleanup]
        public void CleanUpTestData()
        {
            bool correctAnswer = isCompleted && result == 0;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ApproximateElapsedTime.Hours, ApproximateElapsedTime.Minutes, ApproximateElapsedTime.Seconds,
                ApproximateElapsedTime.Milliseconds);
            string errorMSG = "";

            if (result == -2)
                errorMSG = "Exception occured in your code";
            else if (result == -1)
                errorMSG = "Output is incorrect";
           
            bool RTE = result == -2;

            resultWriter.WriteLine(TestContext.TestName + "," + elapsedTime + "," + isCompleted + "," + correctAnswer + "," + RTE + "," + errorMSG);
            resultWriter.Close();
        }
        
        [TestMethod]
        public void JarvisMarchNormalTestCase20Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case20Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 1 (20 Point)");

        }
        [TestMethod]
        public void JarvisMarchNormalTestCase40Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case40Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 2 (40 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase60Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case60Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 3 (60 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase80Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case80Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 4 (80 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase100Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case100Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 5 (100 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase200Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case200Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 6 (200 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase400Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case400Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 7 (400 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase600Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case600Points, 1500, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 8 (600 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase800Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case800Points, 1500, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 9 (800 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase1000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case1000Points, 2000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 10 (1000 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase2000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case2000Points, 6000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 11 (2000 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase3000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case3000Points, 16000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 12 (3000 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase4000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case4000Points, 25000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 13 (4000 Point)");
        }
        [TestMethod]
        public void JarvisMarchNormalTestCase5000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case5000Points, 60000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 14 (5000 Point)");

        }
        [TestMethod]
        public void JarvisMarchNormalTestCase10000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case10000Points, 3 * 60000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 15 (10000 Point)");
        }
        [TestMethod]
        public void JarvisMarchSpecialCase10SamePoints()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase10SamePoints, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 16 (Point)");
        }
        [TestMethod]
        public void JarvisMarchSpecialCaseLine()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseLine, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 17 (Line)");
        }
        [TestMethod]
        public void JarvisMarchSpecialCaseTriangle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseTriangle, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 18 (Triangle)");
        }
        [TestMethod]
        public void JarvisMarchSpecialCaseCircle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseCircle, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 19 (Circle)");
        }
        [TestMethod]
        public void JarvisMarchSpecialCaseConvexPolygon()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseConvexPolygon, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 20 (Convex Polygon)");
        }
    }
}