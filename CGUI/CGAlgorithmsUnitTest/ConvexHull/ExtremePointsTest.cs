﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGAlgorithms.Algorithms.ConvexHull;
using CGAlgorithms;
using CGUtilities;
using System.Collections.Generic;

namespace CGAlgorithmsUnitTest
{
    /// <summary>
    /// 10 test cases, each 10% of the total grade.
    /// Grade is 0.1 for each test case where total algorithm grade is 10.
    /// </summary>
    [TestClass]
    public class ExtremePointsTest : ConvexHullTest
    {
        bool isCompleted;
        int result;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void InitializeTesTData()
        {
            convexHullTester = new ExtremePoints();
            resultWriter = new System.IO.StreamWriter("TestCasesResults.txt", true);
            result = -1;
            isCompleted = false;
        }

        [TestCleanup]
        public void CleanUpTestData()
        {
            bool correctAnswer = isCompleted && result == 0;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ApproximateElapsedTime.Hours, ApproximateElapsedTime.Minutes, ApproximateElapsedTime.Seconds,
                ApproximateElapsedTime.Milliseconds);
            string errorMSG = "";

            if (result == -2)
                errorMSG = "Exception occured in your code";
            else if (result == -1)
                errorMSG = "Output is incorrect";
            
            bool RTE = result == -2;

            resultWriter.WriteLine(TestContext.TestName + "," + elapsedTime + "," + isCompleted + "," + correctAnswer + "," + RTE + "," + errorMSG);
            resultWriter.Close();
        }
        
        [TestMethod]
        public void ExtremePointsNormalTestCase20Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case20Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 1 (20 Point)");
        }

        [TestMethod]
        public void ExtremePointsNormalTestCase40Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case40Points, 10000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 2 (40 Point)");
        }

        [TestMethod]
        public void ExtremePointsNormalTestCase60Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case60Points, 60000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 3 (60 Point)");
        }
        [TestMethod]
        public void ExtremePointsNormalTestCase80Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case80Points, 2 * 60000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 4 (80 Point)");
        }
        [TestMethod]
        public void ExtremePointsNormalTestCase100Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case100Points, 4 * 60000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 5 (100 Point)");
        }
        [TestMethod]
        public void ExtremePointsSpecialCase10SamePoints()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase10SamePoints, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 16 (Point)");
        }

        [TestMethod]
        public void ExtremePointsSpecialCaseLine()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseLine, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 17 (Line)");
        }
        [TestMethod]
        public void ExtremePointsSpecialCaseTriangle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseTriangle, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 18 (Triangle)");
        }
        [TestMethod]
        public void ExtremePointsSpecialCaseCircle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseCircle, 4 * 60000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 19 (Circle)");
        }
        [TestMethod]
        public void ExtremePointsSpecialCaseConvexPolygon()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseConvexPolygon, 10000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 20 (Convex Polygon)");
        }
    }
}
