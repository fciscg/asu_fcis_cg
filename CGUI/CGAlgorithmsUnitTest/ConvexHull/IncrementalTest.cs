﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGAlgorithms.Algorithms.ConvexHull;
using CGAlgorithms;
using CGUtilities;
using System.Collections.Generic;

namespace CGAlgorithmsUnitTest
{
    /// <summary>
    /// 20 Test Cases, each 5% from the total grade.
    /// Grade is 0.05 for each test case where total algorithm grade is 10.
    /// </summary>
    [TestClass]
    public class IncrementalTest : ConvexHullTest
    {
        bool isCompleted;
        int result;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void InitializeTesTData()
        {
            convexHullTester = new Incremental();
            resultWriter = new System.IO.StreamWriter("TestCasesResults.txt", true);
            result = -1;
            isCompleted = false;
        }

        [TestCleanup]
        public void CleanUpTestData()
        {
            bool correctAnswer = isCompleted && result == 0;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ApproximateElapsedTime.Hours, ApproximateElapsedTime.Minutes, ApproximateElapsedTime.Seconds,
                ApproximateElapsedTime.Milliseconds);
            string errorMSG = "";

            if (result == -2)
                errorMSG = "Exception occured in your code";
            else if (result == -1)
                errorMSG = "Output is incorrect";

            bool RTE = result == -2;

            resultWriter.WriteLine(TestContext.TestName + "," + elapsedTime + "," + isCompleted + "," + correctAnswer + "," + RTE + "," + errorMSG);
            resultWriter.Close();
        }
        
        [TestMethod]
        public void IncrementalNormalTestCase20Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case20Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 1 (20 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase40Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case40Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 2 (40 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase60Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case60Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 3 (60 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase80Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case80Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 4 (80 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase100Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case100Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 5 (100 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase200Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case200Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 6 (200 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase400Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case400Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 7 (400 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase600Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case600Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 8 (600 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase800Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case800Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 9 (800 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase1000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case1000Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 10 (1000 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase2000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case2000Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 11 (2000 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase3000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case3000Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 12 (3000 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase4000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case4000Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 13 (4000 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase5000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case5000Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 14 (5000 Point)");
        }
        [TestMethod]
        public void IncrementalNormalTestCase10000Points()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(Case10000Points, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 15 (10000 Point)");
        }
        [TestMethod]
        public void IncrementalSpecialCase10SamePoints()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase10SamePoints, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 16 (Point)");
        }
        [TestMethod]
        public void IncrementalSpecialCaseLine()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseLine, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 17 (Line)");
        }
        [TestMethod]
        public void IncrementalSpecialCaseTriangle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseTriangle, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 18 (Triangle)");
        }
        [TestMethod]
        public void IncrementalSpecialCaseCircle()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseCircle, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 19 (Circle)");
        }
        [TestMethod]
        public void IncrementalSpecialCaseConvexPolygon()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCaseConvexPolygon, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Times out in " + convexHullTester.ToString() + ": Case 20 (Convex Polygon)");
        }
    }
}
