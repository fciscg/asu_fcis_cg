﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CGAlgorithms.Algorithms.SegmentIntersection;

namespace CGAlgorithmsUnitTest.LineIntersection
{
    /// <summary>
    /// 10 Test Cases, each 10% from the total grade.
    /// Grade is 0.1 for each test case where total algorithm grade is 10.
    /// </summary>
    [TestClass]
    public class SweepLineTest : LineIntersectionTest
    {
        bool isCompleted;
        int result;
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void InitializeTesTData()
        {
            lineIntersectionTester = new SweepLine();
            resultWriter = new System.IO.StreamWriter("TestCasesResults.txt", true);
            result = -1;
            isCompleted = false;
        }

        [TestCleanup]
        public void CleanUpTestData()
        {
            bool correctAnswer = isCompleted && result == 0;

            // Format and display the TimeSpan value.
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:000}",
                ApproximateElapsedTime.Hours, ApproximateElapsedTime.Minutes, ApproximateElapsedTime.Seconds,
                ApproximateElapsedTime.Milliseconds);
            string errorMSG = "";

            if (result == -2)
                errorMSG = "Exception occured in your code";
            else if (result == -1)
                errorMSG = "Output is incorrect";

            bool RTE = result == -2;

            resultWriter.WriteLine(TestContext.TestName + "," + elapsedTime + "," + isCompleted + "," + correctAnswer + "," + RTE + "," + errorMSG);
            resultWriter.Close();
        }
        [TestMethod]
        public void SweepLineNormalTestCaseRandom4LinesIntersectingWithRandom4Lines()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(CaseRandom4LinesIntersectingWithRandom4Lines, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": Case 1: (24 Lines Intersecting with 4 other Lines)");
        }

        [TestMethod]
        public void SweepLineNormalTestCaseRandom5GroupsOfLineIntersections()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(CaseRandom5GroupsOfLineIntersections, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": Case 2: (Random 5 Groups Of Line Intersections)");
        }

        [TestMethod]
        public void SweepLineNormalTestCaseRandom4LinesIntersectingTogether()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(CaseRandom4LinesIntersectingTogether, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": Case 3: (Random 4 Lines Intersecting)");
        }

        [TestMethod]
        public void SweepLineNormalTestCaseEasyCase1LineIntersecting5Lines()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(CaseEasyCase1LineIntersecting5Lines, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": Case 4: (Easy Case: Line Intersecting 5 Lines)");
        }

        [TestMethod]
        public void SweepLineNormalTestCaseEasyCase2LinesIntersecting5Lines()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(CaseEasyCase2LinesIntersecting5Lines, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": Case 5: (Easy Case: 2 Lines Intersecting 5 Lines)");
        }

        [TestMethod]
        public void SweepLineSpecialTestCase21HorizontalLinesPerpendicularOn1VerticalLines()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase21HorizontalLinesPerpendicularOn1VerticalLines, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": SpecialCase 1: (21 horizontal lines Perpendicular on a vertical line)");
        }

        [TestMethod]
        public void SweepLineSpecialTestCase21HorizontalLinePerpendicularOn21VerticalLine()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase21HorizontalLinePerpendicularOn21VerticalLine, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": SpecialCase 2: (21 horizontal lines Perpendicular on 21 vertical lines)");
        }

        [TestMethod]
        public void SweepLineSpecialTestCase2PerpendicularLinesIntersectingInStartOfOneOfThem()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase2PerpendicularLinesIntersectingInStartOfOneOfThem, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": SpecialCase 3: (2 Perpendicular Lines Intersecting In Start Of One Of Them)");
        }

        [TestMethod]
        public void SweepLineSpecialTestCase3LinesIntersectingTogetherWithOneIntersectionEndAndStartOf2Lines()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase3LinesIntersectingTogetherWithOneIntersectionEndAndStartOf2Lines, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": SpecialCase 4: (3 Lines Intersecting Together With One Intersection End And Start Of 2 Lines)");
        }

        [TestMethod]
        public void SweepLineSpecialTestCase3LinesEndAndStartOfEachOther()
        {
            result = CGUtilities.TestingUtilities.Limex<int>(SpecialCase3LinesEndAndStartOfEachOther, 1000, out isCompleted, out ApproximateElapsedTime);
            Assert.IsTrue(isCompleted && result == 0, "Fails in " + lineIntersectionTester.ToString() + ": SpecialCase 5: (3 Lines End And Start Of Each Other)");
        }
    }
}
